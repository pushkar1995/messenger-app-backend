const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

//const PORT = process.env.PORT || 3000;

const users = require('./routes/api/users');
const profile = require('./routes/api/profile');
const posts = require('./routes/api/posts');

const app = express();

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// DB Config
const db = require('./config/keys').mongoURI;

//Connect to MongoDB
mongoose
    .connect(db)
    .then(() => console.log('MongoDB connected'))
    .catch(err => console.log(err));

app.get('/', (req,res) => res.send('Helllo mewhooooo'));

//Use Routes
app.use('/api/users', users);
app.use('/api/profile', profile);
app.use('/api/posts', posts)


app.listen(4000, function(){
    // console.log("Seerver is running on Port: " + PORT);
    console.log("Seerver is running on Port: 4000 ");
})
